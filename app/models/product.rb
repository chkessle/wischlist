class Product
  attr_accessor :description, :image_url, :price, :title, :web_id
  
  def self.search(search)
    if search
      find(:all, :conditions => ['title ILIKE ?', "%#{search}%"])
    else
      find(:all)
    end
  end
  
  def self.getItem(web_id)
    
    is = ItemLookup.new( 'ASIN', { 'ItemId' => web_id } )
    is.response_group = ResponseGroup.new( 'Large' )
    
    req = Request.new
    resp = req.search( is )
    
    items = resp.item_lookup_response[0].items[0].item
    
    products = Array.new
    j = 0
    items.each do |item|
      attribs = item.item_attributes[0]
      products[j] = Product.new

      ### Picture
      url = String.try_convert(item.small_image)
      if url
        startIndex = url.index("http")
        endIndex = url.index("height")
      
        products[j].image_url = url[startIndex..endIndex-2]
      else
        products[j].image_url = "http://g-ecx.images-amazon.com/images/G/03/x-site/icons/no-img-sm._AA75_.gif"
      end
      
      products[j].title = attribs.title
      products[j].web_id = item.asin
      
      ### Description
      #if item.editorial_reviews
      #  products[j].description = String.try_convert(item.editorial_reviews)
      #end
      
      ### Price
      #priceString = String.try_convert(item.offer_summary.lowest_new_price)
      #priceString2 = String.try_convert(item.offer_summary.lowest_used_price)
      
      if attribs.list_price
        products[j].price = attribs.list_price[0].formatted_price.to_s + " (neu) <br/>"
=begin        
      elsif priceString
        startIndex = priceString.index("formatted_price =")
        endIndex = priceString.length
        products[j].price = products[j].price.to_s + priceString[startIndex+18..endIndex] + "(neu) <br/>"
      end
      
      if priceString2
        startIndex = priceString2.index("formatted_price =")
        endIndex = priceString2.length
        products[j].price = products[j].price.to_s + priceString2[startIndex+18..endIndex] + "(gebraucht)"
=end   
      end
      
      j = j+1
    end
    
    products
  end
  
  def self.getItems(search)
    
    is = ItemSearch.new( 'All', { 'Keywords' => search, 'Availability' => 'Available' } )
    is.response_group = ResponseGroup.new( 'Medium' )
    
    req = Request.new
    resp = req.search( is, 5 )
    
    #items = resp.item_search_response[0].items[0].item
    items = resp.collect { |r| r.item_search_response[0].items[0].item }.flatten
    
    products = Array.new
    j = 0
    items.each do |item|
      attribs = item.item_attributes[0]
      product = Product.new

      ### Picture
      url = String.try_convert(item.small_image)
      if url
        startIndex = url.index("http")
        endIndex = url.index("height")
      
        product.image_url = url[startIndex..endIndex-2]
      else
        product.image_url = "http://g-ecx.images-amazon.com/images/G/03/x-site/icons/no-img-sm._AA75_.gif"
      end
      
      product.title = attribs.title
      product.web_id = item.asin
      
      ### Description
      #if item.editorial_reviews
      #  products[j].description = String.try_convert(item.editorial_reviews)
      #end
      
      ### Price
      #priceString = String.try_convert(item.offer_summary.lowest_new_price)
      #priceString2 = String.try_convert(item.offer_summary.lowest_used_price)
      
      if attribs.list_price
        product.price = attribs.list_price[0].formatted_price.to_s + " (neu) <br/>"
        
        products[j] = product
=begin        
      elsif priceString
        startIndex = priceString.index("formatted_price =")
        endIndex = priceString.length
        products[j].price = products[j].price.to_s + priceString[startIndex+18..endIndex] + "(neu) <br/>"
      end
      
      if priceString2
        startIndex = priceString2.index("formatted_price =")
        endIndex = priceString2.length
        products[j].price = products[j].price.to_s + priceString2[startIndex+18..endIndex] + "(gebraucht)"
=end      
        j = j+1
      end
    end
    
    products
  end
end
