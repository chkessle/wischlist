class List < ActiveRecord::Base
  has_many :list_items, dependent: :destroy
  
  def add_list_item(web_id)
    
    current_item = list_items.find_by_web_id(web_id)
    
    if current_item
      list_items.each do | item |
        
        if item.web_id == web_id[0]
          item.quantity += 1
          item.save
          return item
        end
      end
    else
      is = ItemLookup.new( 'ASIN', { 'ItemId' => web_id[0] } )
      is.response_group = ResponseGroup.new( 'Small' )
      
      req = Request.new
      resp = req.search( is )
      
      items = resp.item_lookup_response[0].items[0].item
      
      attribs = items[0].item_attributes[0]

      title = String.try_convert(attribs.title)
      
      ### Picture
      is = ItemLookup.new( 'ASIN', { 'ItemId' => web_id[0] } )
      is.response_group = ResponseGroup.new( 'Images' )
      
      req = Request.new
      resp = req.search( is )
      
      items = resp.item_lookup_response[0].items[0].item
      
      image_url = "http://g-ecx.images-amazon.com/images/G/03/x-site/icons/no-img-sm._AA75_.gif"
      url = String.try_convert(items[0].small_image)
      if url
        startIndex = url.index("http")
        endIndex = url.index("height")
      
        image_url = url[startIndex..endIndex-2]
      end
      
      # build method is inherited from ActiveRecord:Base and create in this case
      # a new list_item with given product_id. :web_id => web_id is a 
      # short form for a array entry
      current_item = list_items.build(:web_id => web_id[0], :title => title, :picture_url => image_url)
    end
    current_item
  end
end