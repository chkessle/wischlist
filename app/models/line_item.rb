class LineItem < ActiveRecord::Base
  attr_accessible :cart_id, :list_item_id

  # add foreign_key from order, product and cart to line_item table 
  belongs_to :order
  belongs_to :list_item
  belongs_to :cart
  
  def total_price
    product.price * quantity
  end
end
