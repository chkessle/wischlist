class ListItem < ActiveRecord::Base
  attr_accessible :web_id, :picture_url, :title
  
  belongs_to :list
  
  def get_title
    
    if @listItem.title
      
      puts "############ title known"
      return @listItem.title
    else

      puts "title not known"
      is = ItemLookup.new( 'ASIN', { 'ItemId' => web_id,
               'MerchantId' => 'Amazon' } )
      is.response_group = ResponseGroup.new( 'Small' )
      
      req = Request.new
      resp = req.search( is )
      
      items = resp.item_lookup_response[0].items[0].item
      
      items.each do |item|
        
        attribs = item.item_attributes[0]
        @listItem.title = attribs.title
        
        return @listItem.title
      end     
    end
  end
  
  def get_picture
    
    if @listItem.picture_url
      puts "##################picture url known"
      
      return @listItem.picture_url
    else
      puts "##################picture url not known"
      puts self.picture_url
    
      is = ItemLookup.new( 'ASIN', { 'ItemId' => web_id } )
      is.response_group = ResponseGroup.new( 'Medium' )
      
      req = Request.new
      resp = req.search( is )
      
      items = resp.item_lookup_response[0].items[0].item
      
      items.each do |item|
        
        attribs = item.item_attributes[0]
        url = String.try_convert(item.small_image)
        if url
          
          startIndex = url.index("http")
          endIndex = url.index("height")
        
          @listItem.picture_url = url[startIndex..endIndex-2] 
        
          return @listItem.picture_url 
        else
          
          @listItem.picture_url = "http://g-ecx.images-amazon.com/images/G/03/x-site/icons/no-img-sm._AA75_.gif"
          return @listItem.picture_url
        end
      end
    end
  end
end
  