class Order < ActiveRecord::Base
  attr_accessible :city, :email, :first_name, :last_name, :pay_type, :street, :zipcode
  
  # creates a one_to_many association between order and line_items. destroys line_items if order will be deleted
  has_many :line_items, dependent: :destroy
  
  PAYMENT_TYPES = [ "Check", "Credit card", "Purchase Order" ]
  
  # because of presence attribute validates checks if given attributes are not null (use blank? method)
  validates :first_name, :last_name, :city, :email, :street, :zipcode, presence: true
  validates :pay_type, inclusion: PAYMENT_TYPES
  
  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      item.cart_id = nil
      line_items << item
    end
  end
end
