class Cart < ActiveRecord::Base
  has_many :line_items, dependent: :destroy
  
  def add_product(product_id)
    # find_by_product_id is not created in that application, but ruby
    # notice the find_by and ends with the name of a column. Because of that
    # it generates dynamically a helper method and added it to that class
    current_item = @cart.line_items.find_by_product_id(product_id)
    if current_item
      current_item.quantity += 1
    else
      # build method is inherited from ActiveRecord:Base and create in this case
      # a new line_item with given product_id. product_id => product_id is a 
      # short form for a array entry
      current_item = @cart.line_items.build(product_id: product_id)
    end
    current_item
  end
  
  def total_price
    @cart.line_items.to_a.sum { |item| item.total_price }
  end
end
