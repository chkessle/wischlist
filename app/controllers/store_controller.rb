class StoreController < ApplicationController
  def index
    @products = Product.getItems
    @cart = current_cart
    @list = current_list
  end
end
