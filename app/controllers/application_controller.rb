class ApplicationController < ActionController::Base
  protect_from_forgery

  # private methods are only available for controllers
  private

    def current_cart
      Cart.find(session[:cart_id])
    rescue ActiveRecord::RecordNotFound
      cart = Cart.create
      session[:cart_id] = cart.id
      return cart
    end
      
    def current_list
      List.find(session[:list_id])
    rescue ActiveRecord::RecordNotFound
      list = List.create
      session[:list_id] = list.id
      return list
    end
end
