class SearchController < ApplicationController
  def search
    @products = Product.getItems(params[:search])
    @cart = current_cart
  end
end
