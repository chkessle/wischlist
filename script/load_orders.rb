# run in root folder of current project: rails runner script/load_orders.rb
Order.transaction do
  (1...100).each do |i|
    Order.create(:first_name => "Cust #{i}", :last_name => "omer #{i}", :email => "asdf#{i}@asd.de", :zipcode => "zipcode#{i}", :street => "Street #{i}", :pay_type => "Check", :city => "city#{i}")
  end
end