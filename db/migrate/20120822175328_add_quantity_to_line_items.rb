# was created with rails generate migration add_quantity_to_line_items quantity:integer
# rails notice add_quantity_to_line_items as a pattern of type add_XXX_to_table
class AddQuantityToLineItems < ActiveRecord::Migration
  def change
    add_column :line_items, :quantity, :integer, default: 1
  end
end
