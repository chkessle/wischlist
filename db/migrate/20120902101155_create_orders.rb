class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :first_name
      t.string :last_name
      t.string :street
      t.string :city
      t.string :zipcode
      t.string :email
      t.string :pay_type

      t.timestamps
    end
  end
end
