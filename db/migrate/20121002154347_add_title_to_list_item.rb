class AddTitleToListItem < ActiveRecord::Migration
  def change
    add_column :list_items, :title, :string
    add_column :list_items, :picture_url, :string
  end
end
