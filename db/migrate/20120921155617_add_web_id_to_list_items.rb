class AddWebIdToListItems < ActiveRecord::Migration
  def change
    add_column :list_items, :web_id, :string
  end
end
